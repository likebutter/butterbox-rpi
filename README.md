# Butter Box on Raspberry Pi

This guide is for "Smooth Operators", the local stewards of Butter Boxes deployed in communities around the world.  It's also for Butter Box developers.  Some of this is quite technical, so use whichever parts of this guide you feel comfortable with. We encourage you to reach out to us and the community for help with parts beyond your skill or comfort level.

**Stable Release:** `v0.1.0`

**Beta Release:** `v0.2.0` -- Smaller image.  Uses content packs in lieu of in-image app store, map files.  These docs describe `v0.2.0`.

**Table of Contents**

[[_TOC_]]

## What Is a Butter Box?

A [Butter Box](https://likebutter.app/box) is a Raspberry Pi or other computer running a set of services that makes life offline a bit smoother.  A Butter Box broadcasts its own WiFi and contains:

* A user friendly homepage
* Encrypted [Matrix](https://matrix.org/) chat accessible via [Keanu Weblite](https://gitlab.com/keanuapp/keanuapp-weblite)
* Extensibility via ["Content Packs"](#adding-custom-content-to-the-box-with-content-packs) which can include app stores, maps, websites, educational materials, and more.

## Creating a Butter Box from an Image (recommended; see caveats)

You can use the [Raspberry Pi Imager](https://www.raspberrypi.com/software/) to write a Butter Box image to your own SD card and drop it into your RPi.  When you boot it up, you'll have a fully-functioning Butter Box.

Butter Box images are currently available in [this dropbox folder](https://www.dropbox.com/sh/y6ei9l7ixrrw04q/AAAgRsH-Ird619ioxEmeibNta?dl=0) and via IPFS.  Links to IPFS releases are available on [the Butter Box website](https://likebutter.app/box).  Files are named `bbb-[version]-[language].img`.

### Caveats

This image comes preloaded with public secrets.  Most of these can be changed using the instructions in this document.

Don't use the image method to install if you're in an environment where someone may want to snoop on what you're doing.  Do use it if you're going to test the Butter Box or plan to use it for non-sensitive communications.  Resolving [this issue](https://gitlab.com/likebutter/butterbox-rpi/-/issues/29) will make image-based installs appropriate for sensitive use cases.

## Creating a Butter Box from Scratch

This is a more technical approach that takes more time and a good Internet connection.  However, it lets you build the latest version of the Butter Box and to customize your installation by editing the `buttermeup.sh` script.

### Requirements

* Raspberry Pi
	* We usually test on:
		* Raspberry Pi 4
		* Raspberry Pi 5
		* Raspberry Pi Zero W 2
	* Note, you may have trouble using a Zero W (2) to build the matrix server.  It will also be much, much slower.
* A separate machine for imaging a Raspberry Pi Micro SD card, "your computer".  Depending on what ports your computer has, you may need an adapter to connect to the MicroSD card.
* Internet connection for the Raspberry Pi via ethernet (preferred) or WiFi.  This is only needed during setup.

### Using `butter-gen`

The fastest, easiest way to create a new image is to use [the Butter pi-gen fork](https://gitlab.com/likebutter/butter-gen).  This is the same tool used to create the official Raspberry Pi OS images.  It first builds a minimal OS image, then runs `buttermeup.sh`.

You can build it right on a Debian host, or on a Mac using Docker.  On a Mac, simply run

```
git clone git@gitlab.com:likebutter/butter-gen.git
cd butter-gen
./build-docker
```

If all goes well, `/butter-gen/deploy/` will contain a fresh new image named `image_YYYY_MM_DD-raspios-butter.img.xz` which you can feed right into the Raspberry Pi Imager and run on a 64-bit Raspberry Pi of your choosing.

### Using the `buttermeup.sh` script

1. Image the RPi.
	* Using Raspberry Pi Imager, select the Raspberry Pi OS Lite 64-bit image.
		* If you'd like to use older RPi hardware such as a Pi Zero W 1, you may need to use a 32-bit base image and disable or work around the components of the install that require 64-bit architecture (e.g. the Flutter-based fdroid-webdash).
	* Using advanced settings (Ctrl+Shift+x):
		* Set the hostname to "butterbox"
		* Enable SSH and set a password suitable for your scenario
		* Configure wifi country to match your country.  If you don't want the RPi to
		  automatically connect to your wireless network (i.e. you plan to use
		  ethernet), a dummy SSID/password will do (e.g. SSID: Foo; Password: bar).
	* Write the image to the SD Card.
2. Update the operating system and reboot.
	* `sudo apt-get update && sudo apt-get full-upgrade -y && sudo apt-get
	  install git -y && sudo reboot`
3. Copy this repository onto the RPi.
	* If this repository is already on your computer, `cd` into it then run
	  `rsync -r ./ pi@butterbox.local:/tmp/butter-setup` to copy it to the RPi.
	* Or, if this repository is available remotely, SSH into the RPi and clone
	  the repository `git clone https://gitlab.com/likebutter/butterbox-rpi.git /tmp/butter-setup`.
4. Run the setup script on the RPi via SSH.
	* `sudo bash /tmp/butter-setup/scripts/buttermeup.sh` should do the trick
	* If you find yourself having a flakey connection to the RPi, you can use `tmux` by running `/tmp/butter-setup/scripts/tmuxify.sh` instead of `buttermeup.sh`.  This also shows htop and a tail of the installation logs, which are useful for debugging.
	* You can specify a language and SSID/domain you'd like to use by using flags:
		* `-l` sets language.  `en` is the default, but `es` is also supported.  This affects the default homepage the user is served, but both languages will be available via a selector.  When `es` is chosen, the SSID and default hostname will be comolamantequilla[.lan].
	* You can specify the `-c` flag to have the script automatically copy an image of the fresh install to the `/dev/sda` device.  The file will be named bbb.img (butterbox backup).
		* This device needs to be larger than the disk you've provisioned the RPI onto since it will first be copied in full, then shrunk to eliminate empty space.
		* If you want to use the .img on a Mac, you can format a USB drive as ExFAT, which will be both writable to the RPi and readable on your Mac.
		* This takes about an hour to copy (then shrink) a 32GB image.
5. Install may take an hour or so depending on network and RPi performance.  The RPi will automatically restart, and should broadcast the network "butterbox" or "comolamantequilla" when it comes back up.  If that fails, unplug the RPi, wait 10 seconds, and plug it back in.

## Moderating, configuring, and customizing your Butter Box

### Securing your Butter Box

Aside from [setting appropriate passwords](#setting-appropriate-passwords), remember:

* If the box isn't physically secure, people could tamper with it.  They could replace the softare running on it, or insert a USB stick with viruses or other exploits.  Consider whether yours should be locked up.
* The public room is public. if you need E2E encryption, make sure create a private room, share that room's invite with only trusted parties, and change "Join Permissions" to "Only People Added" once they've arrived.
* If you do enable IPFS, remember that while the UI exposes just simple upload functions, the full API is exposed to any Butter Box user.  This might be appropriate for a small, trusted group, but probably not for the general public.
* Butter Boxes run on MicroSD cards.  These frequently fail, causing all the data on them to be destroyed.  MicroSD cards can also be stolen.  Attackers could also attempt to fill the drive, leaving you with no more available space.  Backup irreplaceable data when possible.
* [Butter Boxes do not run TLS](https://gitlab.com/likebutter/butterbox-rpi/-/issues/11) and their wifi network is open by default.  This means URLs you visit on the box, including the names of rooms you connect to may be exposed to anyone monitoring traffic (see note about changing "Join Permissions" above).

### Setting appropriate passwords

The Butter Box images we distribute have a standard set of passwords.  Because these are shared publicly, they are not secure.  You should change the passwords to prevent unauthorized access to your Butter Box.

* SSH
	* By default, the `pi` user has the password `butterbox-admin`.
	* Change this password by sshing into the pi and running `passwd`.
	* If you'd prefer to use an SSH key, be sure to disable password access once you enable key-based access.
* RaspAP
	* The access point has an administrative interface that can be used to change its settings.
	* Defaults: user: `admin`, password: `secret` (ironically, this is not secret).
	* Change this by logging in at http://butterbox.lan/admin (or http://comolamantequilla.lan/admin for a Spanish language box) and using the Web UI.
* Chat
	* The public room was created by an administrative user called `butterbox-admin`.  The password for this user is also `butterbox-admin`.
	* Change this password by logging into the Butter Box, going to the public chatroom, then visiting your user profile and updating the password.  At your discretion, you may also wish to change the _name_ from `butterbox-admin` so that other users will recognize you.

Remember that anyone with physical access to the Butter Box could tamper with it including resetting the SSH passwords.  If you're worried about that, keep the box locked in a safe location.

### Moderating the public chat room

When you're logged in to the chat room as the administrative user `butterbox-admin`, you can delete inappropriate messages, kick users out of the room, and give administrator privileges to other users.

### Setting a new SSID or adding a password to the WiFi network

Depending on the default language of your Butter Box, the network will be named `butterbox` or `comolamantequilla`.  You can change this using the RaspAP admin interface available at http://butterbox.lan/admin (or http://comolamantequilla.lan/admin for a Spanish language box).  The default username is `admin` and default password is `secret`.

Using the same interface, you can add a password to the network so only those you give the password to can join.  Consider if this is right for your use case.

If you change the SSID or the password, the QR codes found in this documentation and in Butter Box kits we distribute will no longer work.  You'll need to create a new QR code or have folks type the password to be able to join your network.

## Adding custom content to the box with "Content Packs"

The Butter Box will automatically display the contents of a USB drive when plugged in.  You can use this to share your own files or you can use a USB drive to host one or more premade "Content Packs".

If you don't want to use a USB drive, you (or any user) can also simply post files to the chat room to share with other users.

### Premade Content Packs and Content Pack Recipes:

* App Stores
	* You can host an F-Droid app store as a static content pack!
	* Premade content packs may be available.
	* To make your own, build and deploy `fdroid-webdash-butter` (or its upstream, `fdroid-webdash`) with the fdroid repo of your choice placed in `${fdroid-webdash-root}/fdroid/repo/`.
		* [Docs for F-Droid webdash](https://gitlab.com/uniqx/fdroid-webdash)
		* [Docs for fdroidserver](https://gitlab.com/fdroid/fdroidserver)
	* **Recommended:** When the app store is placed in the USB drive's `/appstore` directory, a special button will be displayed on the Butter Box's homepage directing users to the app store.  Putting the app store in any other directory will also work.  Users will have to click through the "Explore USB" file tree to find it.
* OpenStreetMaps
	* By providing access to an OpenStreetMap (OsmAnd) app and the related map files, Android users can get fully offline maps from the Butter Box.
	* **Recommended:** When you have OsmAnd in an App Store within `/appstore`, and you store `.obf.zip` files from [OsmAnd's exports](https://download.osmand.net/list.php) within `/osm-map-files`, the Butter Box will display a special button on the homepage leading to a site with instructions on how to install the app and files.
* Browser-Based Maps
	* You can host a completely offline map with detail down to the building level!.  The maps themselves are based off of OpenStreetMap and the technology to host them off of [PMTiles](https://github.com/protomaps/PMTiles) and [MapLibre](https://maplibre.org/).
	* Here's [a sample Content Pack](https://www.dropbox.com/s/fa12ej1n18r13hy/maps.zip?dl=0) (use the download link to download this `.zip` file) which contains just the map of Madison, WI.  We can generate you a new mapfile for any geography (or you can do it yourself follwing instructions [here](https://docs.protomaps.com/guide/getting-started)).
* Mantequilla Digital Security Repo
	* "This is a collection of Spanish language videos (the "Watch" folder) and guides ("Learn") that teach digital security for various situations."
	* You can download [the zip file](https://drive.google.com/drive/u/0/folders/1gqD0-liFdXrDO4ettGDtCB_emIw2Syku) and unzip it onto a USB drive.
* OpenCourseWare
	* MIT OpenCourseWare offers a static site of their catalog.  You can try it out [online here](https://ocw-content-offline-live-production.s3.amazonaws.com/index.html).
	* It weighs in at over 1TB and lives on Amazon S3.  You can sync it to a USB drive using `aws s3 sync s3://ocw-content-offline-live-production /path/to/your/usb-drive`.
* News
	* You can use a ["Sneakernet"](https://en.wikipedia.org/wiki/Sneakernet) to carry news to the Butter Box and distribute it to other users.
	* Some news sites are already published as a static site and can be loaded directly onto a USB drive.  For example, the [Guardian Project podcast](https://guardianproject.info/podcast/) website is built as a static site which you can download from gitlab ([example](https://gitlab.com/guardianproject/public-content/engarde-podcast/-/jobs/4651713918/artifacts/download?file_type=archive)) and place on a USB drive.
	* You can also use [AnyNews](https://gitlab.com/guardianproject/anynews) to generate a static site while you have an Internet connection, then load that onto the box.
	* Lastly, for websites that aren't built as a static source, you can save them as a static site using tools like [HTTrack](https://www.httrack.com/).

To use a content pack, simply put the files on a USB drive (remember to "unzip" any `.zip` files first).  Then, insert that drive into the Butter Box.

### How to Broadcast content from a USB Drive

If you're using a Pi Zero W 2 or another device, you may need to use an adapter [like this](https://www.amazon.com/gp/product/B00LN3LQKQ/) to plug in a USB drive.

When the drive is plugged in, a new section will appear on the homepage providing the option to view the files an administrator has made available.  Users will be able to browse through subfolders, selecting files by name.  Files which can be natively displayed in a browser, like images, will display with a click, while other files, like .apk app files, will be downloaded to the device.

Unplugging the USB drive will cause the section on the homepage to disappear.

### Distributing your own files

You can put anything you want on your USB drive including a mix of content packs and personal content.

If you want to be fancy, you can include an `index.html` in the root folder or any subfolders and it'll be served instead of a directory listing.  This provides the opportunity for substantial customization.

## Troubleshooting

1. If this is the first time you've plugged in a new Butter Box it may take several minutes before you first see the WiFi network.  After this first boot, it should boot much more quickly.
2. Unplugging the Butter Box, waiting 10 seconds, then plugging it back in will resolve most problems.
3. If you're comfortable in linux, `ssh` into the Butter Box and check the status of the troublesome services.  `systemctl` and `journalctl` are good tools, and the major services are:
	* `lighttpd`: the webserver that hosts the homepage and the admin interface for RaspAP
	* `hostapd`: broadcasts the local access point.
	* `ipfs`: the local IPFS node 
	* `dendrite`: the chat server.
4. You can always re-image your Butter Box using the instructions above.  This will erase any data (e.g. chat, IPFS uploads) and user accounts already on the Butter Box, so only do this as a last resort or if none of your users need data from the past.  If you're unable to do this, reach out to the team who may be able to send you a postcard containing a new MicroSD card.

## Onboard IPFS

Note: This feature is turned off by default.

Butter Box comes with a built-in [IPFS](https://ipfs.tech/) node and a web UI for uploading files to IPFS.  When a user uploads a file, the the local node will pin the file and return the hash of the newly uploaded file to the user.

### Downloading and viewing uploaded files

You can use the IPFS CLI to list the files pinned by the Butter Box.  SSH into the box and use `ipfs pin ls --type=recursive` to see the list of CIDs.  Alternatively, you can use `$ curl -X POST "http://butterbox.lan:5001/api/v0/pin/ls?type=recursive"` from your local machine connected to a butter box to get a JSON formatted list of pins.

To download files, you can use your local IPFS client.  Running `ipfs get <CID>` will download the file to your computer, albeit without an extension.  If you know the filename and extension that should be applied to that CID, great!  If you don't, your browser will often recognize the filetype.  Drag the file into your browser to preview and/or save the file with an appropriate extension.  Alternatively, you can use `➜ curl -X POST "http://butterbox.lan:5001/api/v0/get?arg=<CID>" --output <filename.zip>` to get a ZIP containing the CID's file.

### Making pinned files available on the IPFS mainnet

If (or when) the Butter Box is connected to the Internet, it will make the file available on the main IPFS network.  If you want these CIDs to remain accessible after the Butter Box is disconnected and returned to its offline home, you'll need to ask a pinning service to pin those CIDs.