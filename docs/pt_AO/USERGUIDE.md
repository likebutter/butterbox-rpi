# Manual do Utilizador da Butter Box

## O que é uma Butter Box?

Quando não é possível aceder à Internet, uma Butter Box torna a vida sem internet um pouco mais fácil.  

A Butter Box é um dispositivo ao qual se pode aceder através da ligação
Wi-Fi do seu smartphone ou computador.  Quando estiver ligado, pode instalar
aplicativos Android como o Open Street Maps, um Manual de Sobrevivência ou
mesmo jogos para ocupar os seus filhos (ou a si próprio!).  Também pode
utilizar a Butter Box para conversar em público ou em privado.

## Configuração de uma Butter Box

Basta ligá-la através do adaptador de corrente incluído ou de uma fonte de
alimentação micro USB adequada.  Em poucos segundos, verá que estará a
transmitir a rede wifi "butterbox".

![[Conecte a butter box a uma
tomada](images/plugged-in-to-outlet.jpg){width=40%}

![ Conecte a butter box a uma fonte de energia
solar](images/plugged-in-to-solar.jpg){width=40%}

A Butter Box funciona sem ligação à Internet e vem pré-carregada com tudo o
que precisa.  A primeira vez que a ligar, pode demorar alguns minutos a
arrancar.  Depois disso, o arranque será mais rápido.

## Ligação à Butter Box

Através do telemóvel ou do computador, ligue-se à rede Wi-Fi "butterbox".
Para garantir que o seu dispositivo procure informações na Butter Box e não
na Internet em geral, desligue a ligação celular do seu telemóvel.  Também
pode aderir à rede através deste código QR:

![ Ligação à rede wifi da
butterbox](images/pt/qr/ssid-butterbox.png){width=40%}

De seguida, visite `http://butterbox.lan`.  Para aceder a esse endereço,
basta fazer a leitura deste código QR:

![Visite a página inicial da Butter
Box](images/en/qr/butterbox.lan.png){width=40%}

### Instalação de Aplicativos Android

Depois de se ligar e carregar a página inicial da Butter Box, clique em
"Baixar o Aplicativo Android" e, em seguida, clique em "Baixar o Aplicativo
Android" novamente no modal.  Isto irá transferir um ficheiro chamado
"butter.apk" para o seu dispositivo.

![Download butter.](images/en/download.jpg){width=40%}

Abra o "butter.apk".  Poderá ter de permitir a instalação a partir de fontes
desconhecidas.  Isto irá instalar o Aplicativo Butter, que agora pode ser
aberto.

![O aplicativo butter na página inicial](images/en/app.jpg){width=40%}

Depois do aplicativo Butter carregar o repositório, verá uma lista dos
aplicativos disponíveis.  Clique em qualquer aplicativo para ler mais e, em
seguida, em "Instalar" para instalar o aplicativo no seu dispositivo.

![A lista de aplicativos da loja de aplicativos
butter](images/en/butter-app-store.jpg){width=40%}

### Conversa com outros utilizadores da Butter Box

Depois de se conectar e carregar a página inicial da Butter Box, pode optar
por se juntar a uma sala de conversa pública ou iniciar uma conversa privada
através dos botões na página inicial http://butterbox.lan.

Tal como um quadro de avisos, pode ver todas as mensagens que os outros
deixaram na conversa pública e quaisquer mensagens que deixe na conversa
pública estarão disponíveis para outros que se conectem mais tarde.

Se quiser ter uma conversa privada, pode criar a sua própria sala de conversa.  Quando cria uma sala privada, apenas as pessoas que convidou podem entrar e todas as mensagens que enviar serão codificadas para que apenas você e os seus convidados as possam ver.  Nem mesmo os criadores ou administradores da Butter Box podem ver as suas mensagens privadas.  

Para convidar outras pessoas para a sala de conversa que criou, clique no
ícone no canto superior esquerdo e, em seguida, clique no código QR pequeno
para ampliá-lo de modo a que os seus amigos o possam ler.  Os seus amigos
juntam-se à sala ligando-se à rede Wi-Fi da Butter Box e fazendo a leitura
do código QR.

## Resolução de problemas

Se estiver ligado à rede Wi-Fi, mas não conseguir carregar a página inicial,
certifique-se de que os seus dados móveis estejam desligados.

Se a Butter Box não estiver a funcionar correctamente, o passo mais fácil é
reiniciá-la desligando-a da tomada, aguardando 10 segundos e voltando a
ligá-la.

Se reiniciar a Butter Box não resolver o problema, peça ajuda ao seu
administrador.
