# Butter Box na Raspberry Pi

Este guia destina-se aos "Smooth Operators", os responsáveis locais pelas
Butter Boxes instaladas em comunidades de todo o mundo.  Destina-se, também,
aos programadores das Butter Boxes.  Algumas partes são bastante técnicas,
portanto, use as partes deste guia com as quais se sentir
confortável. Encorajamo-lo a contactar-nos e à comunidade para obter ajuda
em partes que ultrapassem o seu nível de competência ou de conforto.


**Índice**

[[_TOC_]]

## O que é uma Butter Box?

Um aplicativo [Butter Box] (https://likebutter.app/box) é um Raspberry Pi ou
outro computador com um conjunto de serviços que tornam a vida sem internet
um pouco mais fácil.  Uma Butter Box contém:

* Uma página inicial de fácil utilização
* Loja de Aplicativos Butter e o Aplicativo Butter para a aceder
* Conversa codificada [Matrix](https://matrix.org/) acessível através de
  [Keanu Weblite](https://gitlab.com/keanuapp/keanuapp-weblite)

## Criação de uma Butter Box a partir de um ficheiro (recomendado; ver advertências)

É possível usar o [Raspberry Pi Imager
](https://www.raspberrypi.com/software/) para gravar uma imagem do Butter
Box no seu próprio cartão SD e colocá-la no seu RPi.  Quando o arrancares,
terás uma Butter Box a funcionar completamente.

Actualmente, os ficheiros da Butter Box estão disponíveis em [esta pasta
dropbox]
(https://www.dropbox.com/sh/y6ei9l7ixrrw04q/AAAgRsH-Ird619ioxEmeibNta?dl=0)
e via IPFS.  Links para as versões IPFS estão disponíveis em [o website da
Butter Box](https://likebutter.app/box).  Os ficheiros têm o nome
`bbb-[idioma principal]-[commit used to provision].img`.

### Advertências

Este ficheiro vem pré-carregado com segredos públicos.  A maior parte disto
pode ser alterado utilizando as instruções deste documento.

Não utilize o método de ficheiro de imagem para instalar se estiver num
ambiente onde alguém possa tentar bisbilhotar o que está a fazer.  Utilize-o
se for testar a Butter Box ou se tencionar utilizá-la para comunicações não
sensíveis.  A resolução [deste problema]
(https://gitlab.com/likebutter/butterbox-rpi/-/issues/29) tornará as
instalações baseadas em ficheiros de imagem apropriadas para casos de
utilização sensíveis.

## Criação de uma Butter Box a partir do Zero

Esta é uma abordagem mais técnica que requer mais tempo e uma boa conexão à
Internet.  No entanto, permite-lhe obter a última versão da Butter Box e
personalizar a sua instalação através da edição do script `buttermeup.sh`.

### Requisitos

* Raspberry Pi
	* Normalmente desenvolvemos num Raspberry Pi 4 e testamos as imagens no
	  Raspberry Pi Zero W 2.
	* Note que pode ter problemas ao utilizar um Zero W (2) para compilar o
	  matrix server.  Também será muito, muito mais lento.
* Uma máquina separada para a criação de imagens de um cartão Micro SD do
  Raspberry Pi, "o seu computador".  Dependendo das entradas do seu
  computador, pode ser necessário um adaptador para ligar ao cartão MicroSD.
* Ligação à Internet para o Raspberry Pi através de ethernet (de
  preferência) ou WiFi.  Isto só é necessário durante a configuração.

### Utilização do script `buttermeup.sh

1. Visualizar o RPi.
	* Utilização do Raspberry Pi Imager, seleccione a imagem Raspberry Pi OS
	  Lite 64-bit.
		* Se pretende utilizar o hardware RPi mais antigo, como um Pi Zero W 1,
		  poderá ter de utilizar uma imagem de base de 32 bits e desactivar ou
		  contornar os componentes da instalação que requerem uma arquitetura de
		  64 bits (por exemplo, o fdroid-webdash baseado no Flutter).
	* Utilização de definições avançadas (Ctrl+Shift+x):
		* Defina o nome do anfitrião para "butterbox"
		* Active o SSH e defina uma palavra-passe adequada ao seu cenário
		* Configure o país do wifi para corresponder ao seu país.  Se não quiser
		  que o RPi se ligue automaticamente à sua rede wireless (ou seja, se
		  tenciona usar ethernet), um SSID/palavra-passe dummy serve (por exemplo,
		  SSID: Foo; Palavra-passe: bar).
	* Grave a imagem no cartão SD.
2. Atualize o sistema operativo e reinicie-o.
	* `sudo apt-get update && sudo apt-get full-upgrade -y && sudo apt-get
	  install git -y && sudo reboot`
3. Copie este repositório para o RPi.
	* Se este repositório já estiver no seu computador, `cd` nele e em seguida
	  execute `rsync -r ./ pi@butterbox.local:/tmp/butter-setup` para copiá-lo
	  para o RPi.
	* Ou, se este repositório estiver disponível remotamente, SSH no RPi e
	  clone o repositório `git clone
	  https://gitlab.com/likebutter/butterbox-rpi.git /tmp/butter-setup`.
4. Execute o script de configuração no RPi via SSH.
	* O comando `sudo bash /tmp/butter-setup/scripts/buttermeup.sh` é
	  suficiente para o efeito
	* Se estiver com uma conexão instável com a RPi, pode usar o `tmux` rodando
	  `/tmp/butter-setup/scripts/tmuxify.sh` ao invés de `buttermeup.sh`.  Isso
	  também mostra o registo de instalação, que é útil para depuração.
	* Pode especificar um idioma e um SSID/domínio que gostaria de utilizar
	  através da utilização de sinalizadores:
		* `-l` define o idioma.  `en` é o padrão, mas `es` também é suportado.
		  Isso afecta a página inicial padrão que é exibida ao utilizador, mas
		  ambos os idiomas estarão disponíveis através de um botão de selecção.
		  Quando se escolhe `es`, o SSID e o nome de host padrão passam a ser
		  comolamantequilla[.lan].
	* É possível especificar a flag `-c` para que o script copie
	  automaticamente uma imagem da nova instalação para o dispositivo
	  `/dev/sda`.  O arquivo receberá o nome bbb.img (butterbox backup).
		* Este dispositivo tem de ser maior do que o disco para o qual
		  disponibilizou o RPI, uma vez que será primeiro copiado na totalidade e,
		  em seguida, reduzido para eliminar o espaço vazio.
		* Se quiser usar o .img num Mac, pode formatar uma unidade USB como ExFAT,
		  que poderá ser gravada no RPi e poderá ser repruzida no seu Mac.
		* Isto demora cerca de uma hora a copiar (e depois a reduzir) uma imagem
		  de 32 GB.
5. A instalação pode demorar cerca de uma hora, dependendo do desempenho da
   rede e do RPi.  A RPi irá reiniciar automaticamente, e deverá transmitir
   a rede "butterbox" ou "comolamantequilla" quando voltar a funcionar.  Se
   isso falhar, desligue a RPi, espere 10 segundos e ligue-a novamente.

## Activação, configuração e personalização da Butter Box

### Configuração de uma Butter Box

Aside from [setting appropriate passwords](#setting-appropriate-passwords),
remember:

* If the box isn't physically secure, people could tamper with it.  They
  could replace the softare running on it, or insert a USB stick with
  viruses or other exploits.  Consider whether yours should be locked up.
* The public room is public. if you need E2E encryption, make sure create a
  private room, share that room's invite with only trusted parties, and
  change "Join Permissions" to "Only People Added" once they've arrived.
* If you do enable IPFS, remember that while the UI exposes just simple
  upload functions, the full API is exposed to any Butter Box user.  This
  might be appropriate for a small, trusted group, but probably not for the
  general public.
* Butter Boxes run on MicroSD cards.  These frequently fail, causing all the
  data on them to be destroyed.  MicroSD cards can also be stolen.
  Attackers could also attempt to fill the drive, leaving you with no more
  available space.  Backup irreplaceable data when possible.
* [Butter Boxes do not run
  TLS](https://gitlab.com/likebutter/butterbox-rpi/-/issues/11) and their
  wifi network is open by default.  This means URLs you visit on the box,
  including the names of rooms you connect to may be exposed to anyone
  monitoring traffic (see note about changing "Join Permissions" above).

### Definição de palavras-passe adequadas

As imagens da Butter Box que distribuímos têm um conjunto padrão de
palavras-passe.  Estas são partilhadas publicamente, pelo que não são
seguras.  Deve alterar as palavras-passe para evitar o acesso não autorizado
à sua Butter Box.

* SSH
	* Por pré-definição, o utilizador `pi` tem a palavra-passe
	  `butterbox-admin`.
	* Altere esta palavra-passe através de sshing no pi e executando `passwd`.
	* Se preferir utilizar uma chave SSH, certifique-se de que desactivou o
	  acesso por palavra-passe depois de activar o acesso baseado em chave.
* RaspAP
	* O ponto de acesso tem uma interface administrativa que pode ser utilizada
	  para alterar as suas definições.
	* Predefinições: utilizador: `admin`, palavra-passe: `secret`
	  (ironicamente, esta não é secreta).
	* Para mudar esta situação, inicie a sessão em http://butterbox.lan/admin
	  (ou http://comolamantequilla.lan/admin para uma Box em espanhol) e
	  utilize a Web UI.
* Conversa
	* A sala pública foi criada por um utilizador administrativo chamado
	  `butterbox-admin`.  A palavra-passe para este utilizador é também
	  `butterbox-admin`.
	* Altere esta palavra-passe entrando na Butter Box, indo à sala de
	  conversas pública, depois visite o seu perfil de utilizador e actualize a
	  palavra-passe.  Se preferir, pode também mudar o _nome_ de
	  `butterbox-admin` para que os outros utilizadores o reconheçam.

Lembre-se que qualquer pessoa com acesso físico à Butter Box pode
adulterá-la, incluindo a reconfiguração das palavras-passe SSH.  Se estiver
preocupado com isso, mantenha a box trancada num local seguro.

### Ser moderador da sala de conversa pública

Quando estiver ligado à sala de conversa como o utilizador administrativo
`butterbox-admin`, pode apagar mensagens inapropriadas, expulsar
utilizadores da sala e dar privilégios de administrador a outros
utilizadores.

### Definição de um novo SSID ou adição de uma palavra-passe à rede WiFi

Dependendo do idioma predefinido da sua Butter Box, a rede terá o nome
`butterbox` ou `comolamantequilla`.  Pode alterar este nome utilizando a
interface de administrador do RaspAP disponível em
http://butterbox.lan/admin (ou http://comolamantequilla.lan/admin para uma
box de língua espanhola).  O nome de utilizador predefinido é `admin` e a
palavra-passe predefinida é `secret`.

É possível adicionar uma palavra-passe à rede através da utilização da mesma
interface, para que apenas as pessoas a quem a palavra-passe for atribuída
possam aderir.  Pense se isto é adequado para o seu caso de utilização.

Se alterar o SSID ou a palavra-passe, os códigos QR encontrados nesta
documentação e nos kits Butter Box que distribuímos deixarão de funcionar.
Terá de criar um novo código QR ou pedir às pessoas que digitem a
palavra-passe para poderem aderir à sua rede.

### Adição de conteúdo personalizado à box

The Butter Box will automatically display the contents of a USB drive when
plugged in.  You can use this to share your own files or you can use a USB
drive to host one or more premade "Content Packs".

If you don't want to use a USB drive, you (or any user) can also simply post
files to the chat room to share with other users.

#### Premade Content Packs and Content Pack Recipes:

* Maps
	* You can host a completely offline map with detail down to the building
	  level!.  The maps themselves are based off of OpenStreetMap and the
	  technology to host them off of
	  [PMTiles](https://github.com/protomaps/PMTiles) and
	  [MapLibre](https://maplibre.org/).
	* Here's [a sample Content
	  Pack](https://www.dropbox.com/s/fa12ej1n18r13hy/maps.zip?dl=0) (use the
	  download link to download this `.zip` file) which contains just the map
	  of Madison, WI.  We can generate you a new mapfile for any geography (or
	  you can do it yourself follwing instructions
	  [here](https://docs.protomaps.com/guide/getting-started)).
* Mantequilla Digital Security Repo
	* "This is a collection of Spanish language videos (the "Watch" folder) and
	  guides ("Learn") that teach digital security for various situations."
	* You can download [the zip
	  file](https://drive.google.com/drive/u/0/folders/1gqD0-liFdXrDO4ettGDtCB_emIw2Syku)
	  and unzip it onto a USB drive.
* OpenCourseWare
	* MIT OpenCourseWare offers a static site of their catalog.  You can try it
	  out [online
	  here](https://ocw-content-offline-live-production.s3.amazonaws.com/index.html).
	* It weighs in at over 1TB and lives on Amazon S3.  You can sync it to a
	  USB drive using `aws s3 sync s3://ocw-content-offline-live-production
	  /path/to/your/usb-drive`.
* News
	* You can use a ["Sneakernet"](https://en.wikipedia.org/wiki/Sneakernet) to
	  carry news to the Butter Box and distribute it to other users.
	* Some news sites are already published as a static site and can be loaded
	  directly onto a USB drive.  For example, the [Guardian Project
	  podcast](https://guardianproject.info/podcast/) website is built as a
	  static site which you can download from gitlab
	  ([example](https://gitlab.com/guardianproject/public-content/engarde-podcast/-/jobs/4651713918/artifacts/download?file_type=archive))
	  and place on a USB drive.
	* You can also use [AnyNews](https://gitlab.com/guardianproject/anynews) to
	  generate a static site while you have an Internet connection, then load
	  that onto the box.
	* Lastly, for websites that aren't built as a static source, you can save
	  them as a static site using tools like
	  [HTTrack](https://www.httrack.com/).

To use a content pack, simply put the files on a USB drive (remember to
"unzip" any `.zip` files first).  Then, insert that drive into the Butter
Box.

#### How to Broadcast content from a USB Drive

If you're using a Pi Zero W 2 or another device, you may need to use an
adapter [like this](https://www.amazon.com/gp/product/B00LN3LQKQ/) to plug
in a USB drive.

When the drive is plugged in, a new section will appear on the homepage
providing the option to view the files an administrator has made available.
Users will be able to browse through subfolders, selecting files by name.
Files which can be natively displayed in a browser, like images, will
display with a click, while other files, like .apk app files, will be
downloaded to the device.

Unplugging the USB drive will cause the section on the homepage to
disappear.

#### Distributing your own files

You can put anything you want on your USB drive including a mix of content
packs and personal content.

If you want to be fancy, you can include an `index.html` in the root folder
or any subfolders and it'll be served instead of a directory listing.  This
provides the opportunity for substantial customization.

## Resolução de problemas

1. Se for a primeira vez que liga uma Butter Box nova, pode demorar vários
   minutos até ver a rede WiFi pela primeira vez.  Após este primeiro
   arranque, o arranque deverá ser muito mais rápido.
2. Se desligar a Butter Box, esperar 10 segundos e voltar a ligá-la, a
   maioria dos problemas fica resolvida.
3. Se estiver confortável no linux, `ssh` na Butter Box e verifique o estado
   dos serviços problemáticos.  O `systemctl` e o `journalctl` são boas
   ferramentas, e os principais serviços são:
	* `lighttpd`: o servidor web que hospeda a página inicial e a interface de
	  administrador do RaspAP
	* `hostapd`: transmite o ponto de acesso local.
	* ipfs': o IPFS node local
	* `dendrite`: o servidor de conversa.
4. É sempre possível recriar a imagem da Butter Box usando as instruções
   acima.  Isto irá apagar todos os dados (por exemplo, conversas,
   carregamentos IPFS) e contas de utilizador já existentes na Butter Box,
   por isso, faça-o apenas como último recurso ou se nenhum dos seus
   utilizadores precisar de dados do passado.  Se não conseguir fazer isto,
   contacte a equipa, que poderá enviar-lhe um bilhete postal com um novo
   cartão MicroSD.

## Onboard IPFS

Note: This feature is turned off by default.

A Butter Box vem com um [IPFS](https://ipfs.tech/) node incorporado e uma IU
web para carregar ficheiros para o IPFS.  Quando um utilizador carrega um
ficheiro, o node local fixará o ficheiro e devolverá o hash do ficheiro
recentemente carregado ao utilizador.

### Transferir e visualizar ficheiros carregados

É possível usar o IPFS CLI para listar os arquivos fixados pela Butter Box.
SSH na box e utilize `ipfs pin ls --type=recursive` para ver a lista de
CIDs.  Alternativamente, pode-se utilizar `$ curl -X POST
"http://butterbox.lan:5001/api/v0/pin/ls?type=recursive"` a partir de sua
máquina local conectada a uma Butter Box para obter uma lista de pins em
formato JSON.

Para transferir ficheiros, pode utilizar o seu IPFS client local.  Executar `ipfs get <CID>` irá transferir o arquivo para o seu computador, embora sem uma extensão.  Se souber o nome do arquivo e a extensão que deve ser aplicada a esse CID, óptimo!  Se não souber, o seu browser reconhecerá frequentemente o tipo de ficheiro.  Arraste o arquivo para o seu navegador para visualizar e/ou gravar o arquivo com uma extensão apropriada.  Alternativamente, pode-se usar `➜ curl -X POST "http://butterbox.lan:5001/api/v0/get?arg=<CID>" --output <filename.zip>` para obter um ZIP contendo o arquivo do CID.

### Disponibilização de ficheiros fixados na rede principal do IPFS

Se (ou quando) a Butter Box estiver conectada à Internet, disponibilizará o
arquivo na rede IPFS principal.  Se quiser que esses CIDs permaneçam
acessíveis depois que a Butter Box for desconectada e voltar para sua sede
offline, será necessário pedir a um serviço de fixação para fixar esses
CIDs.
