# Boîte Butter sur Raspberry Pi

Le présent guide s'adresse aux « Opérateurs compétents », c'est-à-dire aux
responsables locaux des Boîtes Butter déployées dans les communautés à
travers le monde.  Il s'adresse également aux développeurs des Boîtes
Butter.  Certains points sont assez techniques, alors utilisez les points du
présent guide qui vous conviennent le mieux. Nous vous encourageons à nous
contacter, ainsi que la communauté, pour obtenir de l'aide sur les points
qui dépassent vos compétences ou votre niveau d'aisance.


**Table des matières**

[[_TDM_]]

## Qu'est-ce qu'une Boîte Butter ?

Une [Boîte Butter] (https://likebutter.app/box) est un Raspberry Pi ou un
autre ordinateur qui exécute un ensemble de services qui rendent la vie hors
ligne un peu plus facile.  Une Boîte Butter contient :

* Une page d'accueil conviviale
* La boutique d'applications de Butter et l'application Butter pour y
  accéder
* Des discussions chiffrées [Matrix](https://matrix.org/) accessibles via
  [Keanu Weblite](https://gitlab.com/keanuapp/keanuapp-weblite)

## Création d'une Boîte Butter à partir d'une image (recommandé ; voir les notes d'avertissement)

Vous pouvez utiliser le [Raspberry Pi Imager]
(https://www.raspberrypi.com/software/) pour graver une image de la Boîte
Butter sur votre propre carte SD et l'insérer dans votre RPi.  Lorsque vous
le démarrerez, vous aurez une Boîte Butter entièrement fonctionnelle.

Les images de la Boîte Butter sont actuellement disponibles dans [ce dossier
Dropbox]
(https://www.dropbox.com/sh/y6ei9l7ixrrw04q/AAAgRsH-Ird619ioxEmeibNta?dl=0)
et via IPFS.  Des liens vers les versions IPFS sont disponibles sur [le site
web de la Boîte Butter] (https://likebutter.app/box).  Les fichiers sont
nommés `bbb-[langue principale]-[engagement pris pour la fourniture].img`.

### Notes d'avertissement

Cette image est pré-configurée avec des secrets publics.  La plupart d'entre
eux peuvent être modifiés en suivant les instructions contenues dans ce
document.

N'utilisez pas la méthode de l'image pour l'installation si vous vous
trouvez dans un environnement où quelqu'un pourrait vouloir vous espionner.
Utilisez-la si vous souhaitez tester la Boîte Butter ou si vous prévoyez de
l'utiliser à des fins de communication non sensibles.  La résolution de [ce
problème] (https://gitlab.com/likebutter/butterbox-rpi/-/issues/29) rendra
les installations à partir d'images appropriées pour les cas d'utilisation
sensibles.

## Création d'une Boîte Butter à partir du néant

Il s'agit là d'une approche plus technique qui demande plus de temps et une
bonne connexion Internet.  Cependant, elle vous permet de créer la dernière
version de la Boîte Butter et de personnaliser votre installation en éditant
le script `buttermeup.sh`.

### Exigences

* Raspberry Pi
	* Nous créons généralement nos applications sur un Raspberry Pi 4 et nous
	  testons les images sur un Raspberry Pi Zéro W 2.
	* Attention : vous pourrez rencontrer des difficultés à utiliser un Zéro W
	  (2) pour créer le serveur matriciel.  En outre, les performances du
	  serveur seront beaucoup plus lentes.
* Une machine distincte pour la création d'images sur une carte Micro SD
  Raspberry Pi, « votre ordinateur ».  En fonction des ports dont dispose
  votre ordinateur, vous aurez peut-être besoin d'un adaptateur pour vous
  connecter à la carte MicroSD.
* Connexion Internet pour le Raspberry Pi via Ethernet (de préférence) ou
  Wi-Fi.  Cette connexion n'est nécessaire que lors de la configuration.

### Utilisation du script `buttermeup.sh`

1. Créer l'image du RPi.
	* À l'aide de Raspberry Pi Imager, sélectionnez l'image Raspberry Pi OS
	  Lite 64-bit.
		* Si vous souhaitez utiliser un matériel RPi plus ancien comme un Pi Zéro
		  W 1, vous devrez peut-être utiliser une image de base 32 bits et
		  désactiver ou contourner les composants de l'installation qui
		  nécessitent une architecture 64 bits (par exemple, fdroid-webdash basé
		  sur Flutter).
	* Utilisation des paramètres avancés (Ctrl+Shift+x) :
		* Définissez le nom d'hôte à « butterbox »
		* Activez le protocole SSH et définissez un mot de passe adapté à votre
		  scénario
		* Sélectionnez votre pays en tant que pays du Wi-Fi.  Si vous ne souhaitez
		  pas que le RPi se connecte automatiquement à votre réseau sans fil
		  (c'est-à-dire si vous prévoyez d'utiliser l'Ethernet), un SSID/mot de
		  passe fictif suffira (par exemple SSID : Foo ; Password : bar).
	* Graver l'image sur la carte SD.
2. Mettez le système d'exploitation à jour et redémarrez.
	* `sudo apt-get update && sudo apt-get full-upgrade -y && sudo apt-get
	  install git -y && sudo reboot`
3. Copiez ce dépôt d'applications sur le RPi.
	* Si ce dépôt d'applications existe déjà sur votre ordinateur, `cd` dans
	  celui-ci et exécutez `rsync -r ./ pi@butterbox.local:/tmp/butter-setup`
	  pour le copier sur le RPi.
	* Ou, si ce dépôt d'applications est disponible à distance, connectez-vous
	  en SSH à votre RPi et clonez le dépôt d'applications `git clone
	  https://gitlab.com/likebutter/butterbox-rpi.git /tmp/butter-setup`.
4. Lancez le script d'installation sur le RPi via SSH.
	* `sudo bash /tmp/butter-setup/scripts/buttermeup.sh` devrait faire
	  l'affaire
	* Si vous avez une faible connexion au RPi, vous pouvez utiliser `tmux` en
	  lançant `/tmp/butter-setup/scripts/tmuxify.sh` au lieu de
	  `buttermeup.sh`.  Ceci présente également htop et un extrait des journaux
	  d'installation utiles pour le débogage.
	* Vous pouvez définir une langue et un SSID/domaine que vous souhaitez
	  utiliser à l'aide de drapeaux :
		* `-l` définit la langue.  `en` est la langue par défaut, mais `es` est
		  également supportée.  Cette action affecte la page d'accueil par défaut
		  proposée à l'utilisateur, mais les deux langues seront disponibles par
		  l'intermédiaire d'un sélecteur.  Lorsque `es` est choisie, le SSID et le
		  nom d'hôte par défaut seront comolamantequilla[.lan].
	* Vous pouvez spécifier le drapeau `-c` pour que le script copie
	  automatiquement une image de la nouvelle installation sur le périphérique
	  `/dev/sda`.  Le fichier sera nommé bbb.img (sauvegarde butterbox).
		* Ce périphérique doit être plus volumineux que le disque sur lequel vous
		  avez installé le RPI, car il sera d'abord copié dans son intégralité,
		  puis compressé afin d'éliminer l'espace vide.
		* Si vous souhaitez utiliser le fichier .img sur un Mac, vous pouvez
		  formater une clé USB en ExFAT, qui sera à la fois exploitable par le RPi
		  et exécutable sur votre Mac.
		* Cette opération prend environ une heure pour copier (puis compresser)
		  une image de 32 Go.
5. L'installation peut prendre une heure ou plus selon les performances du
   réseau et du RPi.  Le RPi redémarrera automatiquement, et devrait
   afficher le réseau « butterbox » ou « comolamantequilla » lorsqu'il se
   rallumera.  En cas d'échec, débranchez le RPi, attendez 10 secondes et
   rebranchez-le.

## Modération, configuration et personnalisation de votre Boîte Butter

### Configuration d'une Boîte Butter

Aside from [setting appropriate passwords](#setting-appropriate-passwords),
remember:

* If the box isn't physically secure, people could tamper with it.  They
  could replace the softare running on it, or insert a USB stick with
  viruses or other exploits.  Consider whether yours should be locked up.
* The public room is public. if you need E2E encryption, make sure create a
  private room, share that room's invite with only trusted parties, and
  change "Join Permissions" to "Only People Added" once they've arrived.
* If you do enable IPFS, remember that while the UI exposes just simple
  upload functions, the full API is exposed to any Butter Box user.  This
  might be appropriate for a small, trusted group, but probably not for the
  general public.
* Butter Boxes run on MicroSD cards.  These frequently fail, causing all the
  data on them to be destroyed.  MicroSD cards can also be stolen.
  Attackers could also attempt to fill the drive, leaving you with no more
  available space.  Backup irreplaceable data when possible.
* [Butter Boxes do not run
  TLS](https://gitlab.com/likebutter/butterbox-rpi/-/issues/11) and their
  wifi network is open by default.  This means URLs you visit on the box,
  including the names of rooms you connect to may be exposed to anyone
  monitoring traffic (see note about changing "Join Permissions" above).

### Définition de mots de passe appropriés

Les images de la Boîte Butter que nous distribuons sont assorties d'un
ensemble de mots de passe standard.  Ceux-ci étant partagés publiquement,
ils ne sont pas sécurisés.  Vous devriez changer les mots de passe pour
empêcher tout accès non autorisé à votre Boîte Butter.

* SSH
	* Par défaut, le mot de passe de l'utilisateur `pi` est `butterbox-admin`.
	* Pour changer ce mot de passe, connectez-vous au serveur pi en ssh et
	  exécutez `passwd`.
	* Si vous préférez utiliser une clé SSH, assurez-vous de désactiver l'accès
	  par mot de passe après avoir activé l'accès par clé.
* RaspAP
	* Le point d'accès dispose d'une interface d'administration qui permet de
	  modifier ses paramètres.
	* Valeurs par défaut : utilisateur : `admin`, mot de passe : `secret`
	  (paradoxalement, ce n'est pas un secret).
	* Modifiez ces valeurs en vous connectant à l'adresse
	  http://butterbox.lan/admin (ou http://comolamantequilla.lan/admin pour
	  une boîte en langue espagnole) et en utilisant l'interface Web.
* Discussions
	* L'espace public a été créé par un utilisateur administrateur appelé
	  `butterbox-admin`.  Le mot de passe de cet utilisateur est également
	  `butterbox-admin`.
	* Changez ce mot de passe en vous connectant à la Boîte Butter, en accédant
	  au forum de discussion public, puis en consultant votre profil
	  d'utilisateur et en mettant à jour votre mot de passe.  Si vous le
	  souhaitez, vous pouvez également changer le _nom_ de `butterbox-admin`
	  afin que les autres utilisateurs puissent vous reconnaître.

Rappelez-vous que toute personne ayant un accès physique à la Boîte Butter
peut la modifier, y compris réinitialiser les mots de passe SSH.  Si cela
vous inquiète, gardez la boîte verrouillée dans un endroit sûr.

### Modération de l'espace de discussion public

Lorsque vous êtes connecté au forum de discussion en tant qu'utilisateur
administratif `butterbox-admin`, vous pouvez supprimer les messages
inappropriés, bannir des utilisateurs du forum et donner des privilèges
d'administrateur à d'autres utilisateurs.

### Définition d'un nouveau SSID ou ajout d'un mot de passe au réseau WiFi

Selon la langue par défaut de votre Boîte Butter, le réseau sera nommé
`butterbox` ou `comolamantequilla`.  Vous pouvez modifier ce nom à l'aide de
l'interface administrateur RaspAP disponible à l'adresse
http://butterbox.lan/admin (ou http://comolamantequilla.lan/admin pour une
boîte en espagnol).  Le nom d'utilisateur par défaut est `admin` et le mot
de passe par défaut est `secret`.

En utilisant la même interface, vous pouvez ajouter un mot de passe au
réseau afin que seules les personnes à qui vous avez donné ce mot de passe
puissent s'y connecter.  Rassurez-vous que cette solution soit adaptée à
votre type d'utilisation.

Si vous modifiez le SSID ou le mot de passe, les codes QR figurant dans ce
document et dans les kits de la Boîte Butter que nous distribuons ne
fonctionneront plus.  Vous devrez créer un nouveau code QR ou demander aux
utilisateurs de retaper le mot de passe avant de pouvoir se connecter à
votre réseau.

### Ajout de contenu personnalisé à la boîte

The Butter Box will automatically display the contents of a USB drive when
plugged in.  You can use this to share your own files or you can use a USB
drive to host one or more premade "Content Packs".

If you don't want to use a USB drive, you (or any user) can also simply post
files to the chat room to share with other users.

#### Premade Content Packs and Content Pack Recipes:

* Maps
	* You can host a completely offline map with detail down to the building
	  level!.  The maps themselves are based off of OpenStreetMap and the
	  technology to host them off of
	  [PMTiles](https://github.com/protomaps/PMTiles) and
	  [MapLibre](https://maplibre.org/).
	* Here's [a sample Content
	  Pack](https://www.dropbox.com/s/fa12ej1n18r13hy/maps.zip?dl=0) (use the
	  download link to download this `.zip` file) which contains just the map
	  of Madison, WI.  We can generate you a new mapfile for any geography (or
	  you can do it yourself follwing instructions
	  [here](https://docs.protomaps.com/guide/getting-started)).
* Mantequilla Digital Security Repo
	* "This is a collection of Spanish language videos (the "Watch" folder) and
	  guides ("Learn") that teach digital security for various situations."
	* You can download [the zip
	  file](https://drive.google.com/drive/u/0/folders/1gqD0-liFdXrDO4ettGDtCB_emIw2Syku)
	  and unzip it onto a USB drive.
* OpenCourseWare
	* MIT OpenCourseWare offers a static site of their catalog.  You can try it
	  out [online
	  here](https://ocw-content-offline-live-production.s3.amazonaws.com/index.html).
	* It weighs in at over 1TB and lives on Amazon S3.  You can sync it to a
	  USB drive using `aws s3 sync s3://ocw-content-offline-live-production
	  /path/to/your/usb-drive`.
* News
	* You can use a ["Sneakernet"](https://en.wikipedia.org/wiki/Sneakernet) to
	  carry news to the Butter Box and distribute it to other users.
	* Some news sites are already published as a static site and can be loaded
	  directly onto a USB drive.  For example, the [Guardian Project
	  podcast](https://guardianproject.info/podcast/) website is built as a
	  static site which you can download from gitlab
	  ([example](https://gitlab.com/guardianproject/public-content/engarde-podcast/-/jobs/4651713918/artifacts/download?file_type=archive))
	  and place on a USB drive.
	* You can also use [AnyNews](https://gitlab.com/guardianproject/anynews) to
	  generate a static site while you have an Internet connection, then load
	  that onto the box.
	* Lastly, for websites that aren't built as a static source, you can save
	  them as a static site using tools like
	  [HTTrack](https://www.httrack.com/).

To use a content pack, simply put the files on a USB drive (remember to
"unzip" any `.zip` files first).  Then, insert that drive into the Butter
Box.

#### How to Broadcast content from a USB Drive

If you're using a Pi Zero W 2 or another device, you may need to use an
adapter [like this](https://www.amazon.com/gp/product/B00LN3LQKQ/) to plug
in a USB drive.

When the drive is plugged in, a new section will appear on the homepage
providing the option to view the files an administrator has made available.
Users will be able to browse through subfolders, selecting files by name.
Files which can be natively displayed in a browser, like images, will
display with a click, while other files, like .apk app files, will be
downloaded to the device.

Unplugging the USB drive will cause the section on the homepage to
disappear.

#### Distributing your own files

You can put anything you want on your USB drive including a mix of content
packs and personal content.

If you want to be fancy, you can include an `index.html` in the root folder
or any subfolders and it'll be served instead of a directory listing.  This
provides the opportunity for substantial customization.

## Recherche de pannes

1. Si vous connectez une nouvelle Boîte Butter pour la première fois, cela
   pourrait prendre plusieurs minutes avant que vous puissiez voir le réseau
   Wi-Fi.  Après ce premier démarrage, le démarrage devrait être beaucoup
   plus rapide.
2. Pour résoudre la plupart des problèmes, il suffit de débrancher la Boîte
   Butter, d'attendre 10 secondes, puis la rebrancher.
3. Si vous maîtrisez linux, connectez-vous en `ssh` à la Boîte Butter et
   vérifiez l'état des services qui posent problème.  `systemctl` et
   `journalctl` sont de bons outils, et les principaux services sont :
	* `lighttpd` : le serveur web qui héberge la page d'accueil et l'interface
	  administrateur de RaspAP
	* `hostapd` : diffuse le point d'accès local.
	* `ipfs` : le nœud IPFS local
	* `dendrite` : le serveur de discussion.
4. Vous pouvez toujours recréer l'image de votre Boîte Butter en suivant les
   instructions ci-dessus.  Cette opération supprimera toutes les données
   (p.ex. les discussions, les téléchargements IPFS) et les comptes
   d'utilisateurs existants sur la Boîte Butter. N'effectuez donc cette
   opération qu'en dernier recours, ou si aucun de vos utilisateurs n'a
   besoin de ses anciennes données.  Si vous n'y arrivez pas, contactez
   l'équipe qui pourra vous envoyer une nouvelle carte MicroSD par courrier
   postal.

## Onboard IPFS

Note: This feature is turned off by default.

Butter Box comes with a built-in [IPFS](https://ipfs.tech/) node and a web
UI for uploading files to IPFS.  When a user uploads a file, the the local
node will pin the file and return the hash of the newly uploaded file to the
user.

### Downloading and viewing uploaded files

You can use the IPFS CLI to list the files pinned by the Butter Box.  SSH
into the box and use `ipfs pin ls --type=recursive` to see the list of
CIDs.  Alternatively, you can use `$ curl -X POST
"http://butterbox.lan:5001/api/v0/pin/ls?type=recursive"` from your local
machine connected to a butter box to get a JSON formatted list of pins.

To download files, you can use your local IPFS client.  Running `ipfs get <CID>` will download the file to your computer, albeit without an extension.  If you know the filename and extension that should be applied to that CID, great!  If you don't, your browser will often recognize the filetype.  Drag the file into your browser to preview and/or save the file with an appropriate extension.  Alternatively, you can use `➜ curl -X POST "http://butterbox.lan:5001/api/v0/get?arg=<CID>" --output <filename.zip>` to get a ZIP containing the CID's file.

### Making pinned files available on the IPFS mainnet

If (or when) the Butter Box is connected to the Internet, it will make the
file available on the main IPFS network.  If you want these CIDs to remain
accessible after the Butter Box is disconnected and returned to its offline
home, you'll need to ask a pinning service to pin those CIDs.
