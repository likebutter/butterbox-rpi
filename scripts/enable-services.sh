#!/bin/bash
# Run this only if you provisioned the butterbox in a CHROOT, i.e. with pi-gen

# If the butterbox is provisioned in CHROOT, the services are installed
# but not yet enabled.
services=( "lighttpd" "dendrite" "butterbox-counter" "nodogsplash" "ipfs" "raspapd" "dhcpcd")

apt-get update
apt-get install systemd -y
for service in ${services[@]}; do
    # See if the service exists by seeing if the service file exists
    if [ ! -f "${ROOTFS_DIR}/lib/systemd/system/$service.service" ]; then
        echo "Service $service does not exist.  Skipping."
        continue
    fi
    echo "Enabling service: $service.service"
    systemctl --root=$ROOTFS_DIR enable $service.service
done