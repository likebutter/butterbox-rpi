set -e

# We use nmap to see how many devices are reachable in our subnet
apt-get install nmap -y

cp $butter_src_dir/scripts/counter.sh /usr/bin/butterbox-counter.sh
chmod +x /usr/bin/butterbox-counter.sh

touch /var/log/butter/client-count.log
chown pi:pi /var/log/butter/client-count.log
chmod ug+rw /var/log/butter/client-count.log
chmod o+r /var/log/butter/client-count.log

# Set up a service to automatically run chat on startup.
cp $butter_src_dir/configs/butterbox-counter.service /lib/systemd/system/butterbox-counter.service
systemctl enable butterbox-counter.service
systemctl start butterbox-counter.service