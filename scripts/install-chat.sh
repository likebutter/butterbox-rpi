set -e

# Install dendrite and keanu-weblite

# Install golang
wget -O /tmp/go.tar.gz https://go.dev/dl/go1.22.4.linux-arm64.tar.gz
tar -C /usr/local -xzf /tmp/go.tar.gz
echo "PATH=$PATH:/usr/local/go/bin"  | tee -a /home/pi/.profile
echo "GOPATH=$HOME/golang" | tee -a /home/pi/.profile
source /home/pi/.profile

if [ -d "/home/pi/dendrite" ]; then
  rm -rf /home/pi/dendrite
fi
git clone -b v0.13.7 https://github.com/matrix-org/dendrite /home/pi/dendrite
cd /home/pi/dendrite
/usr/local/go/bin/go build -o bin/ ./cmd/...


# Generate a Matrix signing key for federation (required)
./bin/generate-keys --private-key matrix_key.pem

# Generate a self-signed certificate (optional, but a valid TLS certificate is normally
# needed for Matrix federation/clients to work properly!)
./bin/generate-keys --tls-cert server.crt --tls-key server.key

# Copy and modify the config file - you'll need to set a server name and paths to the keys
# at the very least, along with setting up the database connection strings.
cp $butter_src_dir/configs/butterbox-dendrite.conf /home/pi/dendrite
sed -i "s/REPLACEME/$butter_name/g" /home/pi/dendrite/butterbox-dendrite.conf

# Log directory for pi user
mkdir -p /var/log/dendrite
chown -R pi:pi /var/log/dendrite

# Make sure the server can be run by the pi user.
chown -R pi:pi /home/pi/dendrite

# Set up a service to automatically run chat on startup.
cp $butter_src_dir/configs/butterbox-dendrite.service /lib/systemd/system/dendrite.service
systemctl enable dendrite.service
systemctl start dendrite.service 

# Matrix is available on :8008, which would be prohibited by CORS.  Redirect
# requests on :80 which begin with _matrix to :8008.
cp $butter_src_dir/configs/50-matrix-reverse-proxy.conf /etc/lighttpd/conf-available/
if [ -L "/etc/lighttpd/conf-enabled/50-matrix-reverse-proxy.conf" ]; then
  rm "/etc/lighttpd/conf-enabled/50-matrix-reverse-proxy.conf"
fi
ln -s "/etc/lighttpd/conf-available/50-matrix-reverse-proxy.conf" "/etc/lighttpd/conf-enabled/50-matrix-reverse-proxy.conf"

# Install & build keanu-weblite
# TODO: use main instead of dev.  At this time, dev has a hotfix we need.
if [ -d "/tmp/keanu-weblite" ]; then
  rm -rf /tmp/keanu-weblite
fi
git clone -b dev https://gitlab.com/keanuapp/keanuapp-weblite.git /tmp/keanu-weblite
apt-get install npm -y

cd /tmp/keanu-weblite
npm install
cp $butter_src_dir/configs/keanu-weblite-config.json /tmp/keanu-weblite/src/assets/config.json
sed -i "s/REPLACEME/$butter_name/g" /tmp/keanu-weblite/src/assets/config.json
# See https://gitlab.com/keanuapp/keanuapp-weblite/-/blob/dev/.gitlab-ci.yml?ref_type=heads
export NODE_OPTIONS=--openssl-legacy-provider
npm run build

cp -r /tmp/keanu-weblite/dist /var/www/html/chat

# dependencies for creating the room
apt-get install python3-pip python3.11-venv -y
python -m venv /home/pi/butter
source /home/pi/butter/bin/activate
pip install matrix-nio

# Make this work both in an ordinary and CHROOT environment
# First, try to restart the service.  If it succeeds, we're done.
# If not, workaround to ge the services up and running.

# This succeeds, but is inert, within on_chroot
systemctl restart lighttpd

command="curl localhost/_matrix"
if $command; then
  echo "lighttpd restarted successfully"
  INCHROOT=0
else
  echo "lighttpd restart failed.  Trying to start it."
  INCHROOT=1
  service lighttpd start
  su - pi -c "nohup /home/pi/dendrite/bin/dendrite --config /home/pi/dendrite/butterbox-dendrite.conf -really-enable-open-registration > /var/log/butter/dendrite-provision.log 2>&1 &"
  sleep 3
fi

python $butter_src_dir/scripts/create_public_room.py $butter_language $butter_name

if [ $INCHROOT -eq 1 ]; then
  pkill dendrite
  service lighttpd stop
fi