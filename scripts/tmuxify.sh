# Helpful for local development or debugging troublesome builds
# Runs the install script, tails the logs and shows htop all at once.

sudo apt-get install tmux -y

# Get the directory where this script is located
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

tmux new-session \; \
  send-keys 'sleep 2 && tail -f /var/log/butter/install.txt' C-m \; \
  split-window -v \; \
  send-keys "sudo -E bash ${SCRIPT_DIR}/buttermeup.sh -l en" C-m \; \
  split-window -h \; \
  send-keys 'htop' C-m \; 
