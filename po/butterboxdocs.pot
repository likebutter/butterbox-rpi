# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2024-06-12 14:35-0500\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: en_US\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Title #
#: README.md
#, markdown-text, no-wrap
msgid "Butter Box on Raspberry Pi"
msgstr ""

#. type: Plain text
#: README.md
#, markdown-text
msgid "This guide is for \"Smooth Operators\", the local stewards of Butter Boxes deployed in communities around the world.  It's also for Butter Box developers.  Some of this is quite technical, so use whichever parts of this guide you feel comfortable with. We encourage you to reach out to us and the community for help with parts beyond your skill or comfort level."
msgstr ""

#. type: Plain text
#: README.md
#, markdown-text, no-wrap
msgid "**Table of Contents**\n"
msgstr ""

#. type: Plain text
#: README.md
#, markdown-text
msgid "[[_TOC_]]"
msgstr ""

#. type: Title ##
#: README.md
#, markdown-text, no-wrap
msgid "What Is a Butter Box?"
msgstr ""

#. type: Plain text
#: README.md
#, markdown-text
msgid "A [Butter Box](https://likebutter.app/box) is a Raspberry Pi or other computer running a set of services that makes life offline a bit smoother.  A Butter Box contains:"
msgstr ""

#. type: Bullet: '* '
#: README.md
#, markdown-text
msgid "A user friendly homepage"
msgstr ""

#. type: Bullet: '* '
#: README.md
#, markdown-text
msgid "The Butter App Store & the Butter App for accessing it"
msgstr ""

#. type: Bullet: '* '
#: README.md
#, markdown-text
msgid "Encrypted [Matrix](https://matrix.org/) chat accessible via [Keanu Weblite](https://gitlab.com/keanuapp/keanuapp-weblite)"
msgstr ""

#. type: Title ##
#: README.md
#, markdown-text, no-wrap
msgid "Creating a Butter Box from an Image (recommended; see caveats)"
msgstr ""

#. type: Plain text
#: README.md
#, markdown-text
msgid "You can use the [Raspberry Pi Imager](https://www.raspberrypi.com/software/) to write a Butter Box image to your own SD card and drop it into your RPi.  When you boot it up, you'll have a fully-functioning Butter Box."
msgstr ""

#. type: Plain text
#: README.md
#, markdown-text
msgid "Butter Box images are currently available in [this dropbox folder](https://www.dropbox.com/sh/y6ei9l7ixrrw04q/AAAgRsH-Ird619ioxEmeibNta?dl=0) and via IPFS.  Links to IPFS releases are available on [the Butter Box website](https://likebutter.app/box).  Files are named `bbb-[primary language]-[commit used to provision].img`."
msgstr ""

#. type: Title ###
#: README.md
#, markdown-text, no-wrap
msgid "Caveats"
msgstr ""

#. type: Plain text
#: README.md
#, markdown-text
msgid "This image comes preloaded with public secrets.  Most of these can be changed using the instructions in this document."
msgstr ""

#. type: Plain text
#: README.md
#, markdown-text
msgid "Don't use the image method to install if you're in an environment where someone may want to snoop on what you're doing.  Do use it if you're going to test the Butter Box or plan to use it for non-sensitive communications.  Resolving [this issue](https://gitlab.com/likebutter/butterbox-rpi/-/issues/29) will make image-based installs appropriate for sensitive use cases."
msgstr ""

#. type: Title ##
#: README.md
#, markdown-text, no-wrap
msgid "Creating a Butter Box from Scratch"
msgstr ""

#. type: Plain text
#: README.md
#, markdown-text
msgid "This is a more technical approach that takes more time and a good Internet connection.  However, it lets you build the latest version of the Butter Box and to customize your installation by editing the `buttermeup.sh` script."
msgstr ""

#. type: Title ###
#: README.md
#, markdown-text, no-wrap
msgid "Requirements"
msgstr ""

#. type: Bullet: '* '
#: README.md
#, markdown-text
msgid "Raspberry Pi"
msgstr ""

#. type: Bullet: '	* '
#: README.md
#, markdown-text
msgid "We usually build on a Raspberry Pi 4 and test images on Raspberry Pi Zero W 2."
msgstr ""

#. type: Bullet: '	* '
#: README.md
#, markdown-text
msgid "Note, you may have trouble using a Zero W (2) to build the matrix server.  It will also be much, much slower."
msgstr ""

#. type: Bullet: '* '
#: README.md
#, markdown-text
msgid "A separate machine for imaging a Raspberry Pi Micro SD card, \"your computer\".  Depending on what ports your computer has, you may need an adapter to connect to the MicroSD card."
msgstr ""

#. type: Bullet: '* '
#: README.md
#, markdown-text
msgid "Internet connection for the Raspberry Pi via ethernet (preferred) or WiFi.  This is only needed during setup."
msgstr ""

#. type: Title ###
#: README.md
#, markdown-text, no-wrap
msgid "Using the `buttermeup.sh` script"
msgstr ""

#. type: Bullet: '1. '
#: README.md
#, markdown-text
msgid "Image the RPi."
msgstr ""

#. type: Bullet: '	* '
#: README.md
#, markdown-text
msgid "Using Raspberry Pi Imager, select the Raspberry Pi OS Lite 64-bit image."
msgstr ""

#. type: Bullet: '		* '
#: README.md
#, markdown-text
msgid "If you'd like to use older RPi hardware such as a Pi Zero W 1, you may need to use a 32-bit base image and disable or work around the components of the install that require 64-bit architecture (e.g. the Flutter-based fdroid-webdash)."
msgstr ""

#. type: Bullet: '	* '
#: README.md
#, markdown-text
msgid "Using advanced settings (Ctrl+Shift+x):"
msgstr ""

#. type: Bullet: '		* '
#: README.md
#, markdown-text
msgid "Set the hostname to \"butterbox\""
msgstr ""

#. type: Bullet: '		* '
#: README.md
#, markdown-text
msgid "Enable SSH and set a password suitable for your scenario"
msgstr ""

#. type: Bullet: '		* '
#: README.md
#, markdown-text
msgid "Configure wifi country to match your country.  If you don't want the RPi to automatically connect to your wireless network (i.e. you plan to use ethernet), a dummy SSID/password will do (e.g. SSID: Foo; Password: bar)."
msgstr ""

#. type: Bullet: '	* '
#: README.md
#, markdown-text
msgid "Write the image to the SD Card."
msgstr ""

#. type: Bullet: '2. '
#: README.md
#, markdown-text
msgid "Update the operating system and reboot."
msgstr ""

#. type: Bullet: '	* '
#: README.md
#, markdown-text
msgid "`sudo apt-get update && sudo apt-get full-upgrade -y && sudo apt-get install git -y && sudo reboot`"
msgstr ""

#. type: Bullet: '3. '
#: README.md
#, markdown-text
msgid "Copy this repository onto the RPi."
msgstr ""

#. type: Bullet: '	* '
#: README.md
#, markdown-text
msgid "If this repository is already on your computer, `cd` into it then run `rsync -r ./ pi@butterbox.local:/tmp/butter-setup` to copy it to the RPi."
msgstr ""

#. type: Bullet: '	* '
#: README.md
#, markdown-text
msgid "Or, if this repository is available remotely, SSH into the RPi and clone the repository `git clone https://gitlab.com/likebutter/butterbox-rpi.git /tmp/butter-setup`."
msgstr ""

#. type: Bullet: '4. '
#: README.md
#, markdown-text
msgid "Run the setup script on the RPi via SSH."
msgstr ""

#. type: Bullet: '	* '
#: README.md
#, markdown-text
msgid "`sudo bash /tmp/butter-setup/scripts/buttermeup.sh` should do the trick"
msgstr ""

#. type: Bullet: '	* '
#: README.md
#, markdown-text
msgid "If you find yourself having a flakey connection to the RPi, you can use `tmux` by running `/tmp/butter-setup/scripts/tmuxify.sh` instead of `buttermeup.sh`.  This also shows htop and a tail of the installation logs, which are useful for debugging."
msgstr ""

#. type: Bullet: '	* '
#: README.md
#, markdown-text
msgid "You can specify a language and SSID/domain you'd like to use by using flags:"
msgstr ""

#. type: Bullet: '		* '
#: README.md
#, markdown-text
msgid "`-l` sets language.  `en` is the default, but `es` is also supported.  This affects the default homepage the user is served, but both languages will be available via a selector.  When `es` is chosen, the SSID and default hostname will be comolamantequilla[.lan]."
msgstr ""

#. type: Bullet: '	* '
#: README.md
#, markdown-text
msgid "You can specify the `-c` flag to have the script automatically copy an image of the fresh install to the `/dev/sda` device.  The file will be named bbb.img (butterbox backup)."
msgstr ""

#. type: Bullet: '		* '
#: README.md
#, markdown-text
msgid "This device needs to be larger than the disk you've provisioned the RPI onto since it will first be copied in full, then shrunk to eliminate empty space."
msgstr ""

#. type: Bullet: '		* '
#: README.md
#, markdown-text
msgid "If you want to use the .img on a Mac, you can format a USB drive as ExFAT, which will be both writable to the RPi and readable on your Mac."
msgstr ""

#. type: Bullet: '		* '
#: README.md
#, markdown-text
msgid "This takes about an hour to copy (then shrink) a 32GB image."
msgstr ""

#. type: Bullet: '5. '
#: README.md
#, markdown-text
msgid "Install may take an hour or so depending on network and RPi performance.  The RPi will automatically restart, and should broadcast the network \"butterbox\" or \"comolamantequilla\" when it comes back up.  If that fails, unplug the RPi, wait 10 seconds, and plug it back in."
msgstr ""

#. type: Title ##
#: README.md
#, markdown-text, no-wrap
msgid "Moderating, configuring, and customizing your Butter Box"
msgstr ""

#. type: Title ###
#: README.md
#, markdown-text, no-wrap
msgid "Securing your Butter Box"
msgstr ""

#. type: Plain text
#: README.md
#, markdown-text
msgid "Aside from [setting appropriate passwords](#setting-appropriate-passwords), remember:"
msgstr ""

#. type: Bullet: '* '
#: README.md
#, markdown-text
msgid "If the box isn't physically secure, people could tamper with it.  They could replace the softare running on it, or insert a USB stick with viruses or other exploits.  Consider whether yours should be locked up."
msgstr ""

#. type: Bullet: '* '
#: README.md
#, markdown-text
msgid "The public room is public. if you need E2E encryption, make sure create a private room, share that room's invite with only trusted parties, and change \"Join Permissions\" to \"Only People Added\" once they've arrived."
msgstr ""

#. type: Bullet: '* '
#: README.md
#, markdown-text
msgid "If you do enable IPFS, remember that while the UI exposes just simple upload functions, the full API is exposed to any Butter Box user.  This might be appropriate for a small, trusted group, but probably not for the general public."
msgstr ""

#. type: Bullet: '* '
#: README.md
#, markdown-text
msgid "Butter Boxes run on MicroSD cards.  These frequently fail, causing all the data on them to be destroyed.  MicroSD cards can also be stolen.  Attackers could also attempt to fill the drive, leaving you with no more available space.  Backup irreplaceable data when possible."
msgstr ""

#. type: Bullet: '* '
#: README.md
#, markdown-text
msgid "[Butter Boxes do not run TLS](https://gitlab.com/likebutter/butterbox-rpi/-/issues/11) and their wifi network is open by default.  This means URLs you visit on the box, including the names of rooms you connect to may be exposed to anyone monitoring traffic (see note about changing \"Join Permissions\" above)."
msgstr ""

#. type: Title ###
#: README.md
#, markdown-text, no-wrap
msgid "Setting appropriate passwords"
msgstr ""

#. type: Plain text
#: README.md
#, markdown-text
msgid "The Butter Box images we distribute have a standard set of passwords.  Because these are shared publicly, they are not secure.  You should change the passwords to prevent unauthorized access to your Butter Box."
msgstr ""

#. type: Bullet: '* '
#: README.md
#, markdown-text
msgid "SSH"
msgstr ""

#. type: Bullet: '	* '
#: README.md
#, markdown-text
msgid "By default, the `pi` user has the password `butterbox-admin`."
msgstr ""

#. type: Bullet: '	* '
#: README.md
#, markdown-text
msgid "Change this password by sshing into the pi and running `passwd`."
msgstr ""

#. type: Bullet: '	* '
#: README.md
#, markdown-text
msgid "If you'd prefer to use an SSH key, be sure to disable password access once you enable key-based access."
msgstr ""

#. type: Bullet: '* '
#: README.md
#, markdown-text
msgid "RaspAP"
msgstr ""

#. type: Bullet: '	* '
#: README.md
#, markdown-text
msgid "The access point has an administrative interface that can be used to change its settings."
msgstr ""

#. type: Bullet: '	* '
#: README.md
#, markdown-text
msgid "Defaults: user: `admin`, password: `secret` (ironically, this is not secret)."
msgstr ""

#. type: Bullet: '	* '
#: README.md
#, markdown-text
msgid "Change this by logging in at http://butterbox.lan/admin (or http://comolamantequilla.lan/admin for a Spanish language box) and using the Web UI."
msgstr ""

#. type: Bullet: '* '
#: README.md
#, markdown-text
msgid "Chat"
msgstr ""

#. type: Bullet: '	* '
#: README.md
#, markdown-text
msgid "The public room was created by an administrative user called `butterbox-admin`.  The password for this user is also `butterbox-admin`."
msgstr ""

#. type: Bullet: '	* '
#: README.md
#, markdown-text
msgid "Change this password by logging into the Butter Box, going to the public chatroom, then visiting your user profile and updating the password.  At your discretion, you may also wish to change the _name_ from `butterbox-admin` so that other users will recognize you."
msgstr ""

#. type: Plain text
#: README.md
#, markdown-text
msgid "Remember that anyone with physical access to the Butter Box could tamper with it including resetting the SSH passwords.  If you're worried about that, keep the box locked in a safe location."
msgstr ""

#. type: Title ###
#: README.md
#, markdown-text, no-wrap
msgid "Moderating the public chat room"
msgstr ""

#. type: Plain text
#: README.md
#, markdown-text
msgid "When you're logged in to the chat room as the administrative user `butterbox-admin`, you can delete inappropriate messages, kick users out of the room, and give administrator privileges to other users."
msgstr ""

#. type: Title ###
#: README.md
#, markdown-text, no-wrap
msgid "Setting a new SSID or adding a password to the WiFi network"
msgstr ""

#. type: Plain text
#: README.md
#, markdown-text
msgid "Depending on the default language of your Butter Box, the network will be named `butterbox` or `comolamantequilla`.  You can change this using the RaspAP admin interface available at http://butterbox.lan/admin (or http://comolamantequilla.lan/admin for a Spanish language box).  The default username is `admin` and default password is `secret`."
msgstr ""

#. type: Plain text
#: README.md
#, markdown-text
msgid "Using the same interface, you can add a password to the network so only those you give the password to can join.  Consider if this is right for your use case."
msgstr ""

#. type: Plain text
#: README.md
#, markdown-text
msgid "If you change the SSID or the password, the QR codes found in this documentation and in Butter Box kits we distribute will no longer work.  You'll need to create a new QR code or have folks type the password to be able to join your network."
msgstr ""

#. type: Title ###
#: README.md
#, markdown-text, no-wrap
msgid "Adding custom content to the box"
msgstr ""

#. type: Plain text
#: README.md
#, markdown-text
msgid "The Butter Box will automatically display the contents of a USB drive when plugged in.  You can use this to share your own files or you can use a USB drive to host one or more premade \"Content Packs\"."
msgstr ""

#. type: Plain text
#: README.md
#, markdown-text
msgid "If you don't want to use a USB drive, you (or any user) can also simply post files to the chat room to share with other users."
msgstr ""

#. type: Title ####
#: README.md
#, markdown-text, no-wrap
msgid "Premade Content Packs and Content Pack Recipes:"
msgstr ""

#. type: Bullet: '* '
#: README.md
#, markdown-text
msgid "Maps"
msgstr ""

#. type: Bullet: '	* '
#: README.md
#, markdown-text
msgid "You can host a completely offline map with detail down to the building level!.  The maps themselves are based off of OpenStreetMap and the technology to host them off of [PMTiles](https://github.com/protomaps/PMTiles) and [MapLibre](https://maplibre.org/)."
msgstr ""

#. type: Bullet: '	* '
#: README.md
#, markdown-text
msgid "Here's [a sample Content Pack](https://www.dropbox.com/s/fa12ej1n18r13hy/maps.zip?dl=0) (use the download link to download this `.zip` file) which contains just the map of Madison, WI.  We can generate you a new mapfile for any geography (or you can do it yourself follwing instructions [here](https://docs.protomaps.com/guide/getting-started))."
msgstr ""

#. type: Bullet: '* '
#: README.md
#, markdown-text
msgid "Mantequilla Digital Security Repo"
msgstr ""

#. type: Bullet: '	* '
#: README.md
#, markdown-text
msgid "\"This is a collection of Spanish language videos (the \"Watch\" folder) and guides (\"Learn\") that teach digital security for various situations.\""
msgstr ""

#. type: Bullet: '	* '
#: README.md
#, markdown-text
msgid "You can download [the zip file](https://drive.google.com/drive/u/0/folders/1gqD0-liFdXrDO4ettGDtCB_emIw2Syku) and unzip it onto a USB drive."
msgstr ""

#. type: Bullet: '* '
#: README.md
#, markdown-text
msgid "OpenCourseWare"
msgstr ""

#. type: Bullet: '	* '
#: README.md
#, markdown-text
msgid "MIT OpenCourseWare offers a static site of their catalog.  You can try it out [online here](https://ocw-content-offline-live-production.s3.amazonaws.com/index.html)."
msgstr ""

#. type: Bullet: '	* '
#: README.md
#, markdown-text
msgid "It weighs in at over 1TB and lives on Amazon S3.  You can sync it to a USB drive using `aws s3 sync s3://ocw-content-offline-live-production /path/to/your/usb-drive`."
msgstr ""

#. type: Bullet: '* '
#: README.md
#, markdown-text
msgid "News"
msgstr ""

#. type: Bullet: '	* '
#: README.md
#, markdown-text
msgid "You can use a [\"Sneakernet\"](https://en.wikipedia.org/wiki/Sneakernet) to carry news to the Butter Box and distribute it to other users."
msgstr ""

#. type: Bullet: '	* '
#: README.md
#, markdown-text
msgid "Some news sites are already published as a static site and can be loaded directly onto a USB drive.  For example, the [Guardian Project podcast](https://guardianproject.info/podcast/) website is built as a static site which you can download from gitlab ([example](https://gitlab.com/guardianproject/public-content/engarde-podcast/-/jobs/4651713918/artifacts/download?file_type=archive)) and place on a USB drive."
msgstr ""

#. type: Bullet: '	* '
#: README.md
#, markdown-text
msgid "You can also use [AnyNews](https://gitlab.com/guardianproject/anynews) to generate a static site while you have an Internet connection, then load that onto the box."
msgstr ""

#. type: Bullet: '	* '
#: README.md
#, markdown-text
msgid "Lastly, for websites that aren't built as a static source, you can save them as a static site using tools like [HTTrack](https://www.httrack.com/)."
msgstr ""

#. type: Plain text
#: README.md
#, markdown-text
msgid "To use a content pack, simply put the files on a USB drive (remember to \"unzip\" any `.zip` files first).  Then, insert that drive into the Butter Box."
msgstr ""

#. type: Title ####
#: README.md
#, markdown-text, no-wrap
msgid "How to Broadcast content from a USB Drive"
msgstr ""

#. type: Plain text
#: README.md
#, markdown-text
msgid "If you're using a Pi Zero W 2 or another device, you may need to use an adapter [like this](https://www.amazon.com/gp/product/B00LN3LQKQ/) to plug in a USB drive."
msgstr ""

#. type: Plain text
#: README.md
#, markdown-text
msgid "When the drive is plugged in, a new section will appear on the homepage providing the option to view the files an administrator has made available.  Users will be able to browse through subfolders, selecting files by name.  Files which can be natively displayed in a browser, like images, will display with a click, while other files, like .apk app files, will be downloaded to the device."
msgstr ""

#. type: Plain text
#: README.md
#, markdown-text
msgid "Unplugging the USB drive will cause the section on the homepage to disappear."
msgstr ""

#. type: Title ####
#: README.md
#, markdown-text, no-wrap
msgid "Distributing your own files"
msgstr ""

#. type: Plain text
#: README.md
#, markdown-text
msgid "You can put anything you want on your USB drive including a mix of content packs and personal content."
msgstr ""

#. type: Plain text
#: README.md
#, markdown-text
msgid "If you want to be fancy, you can include an `index.html` in the root folder or any subfolders and it'll be served instead of a directory listing.  This provides the opportunity for substantial customization."
msgstr ""

#. type: Title ##
#: README.md USERGUIDE.md
#, markdown-text, no-wrap
msgid "Troubleshooting"
msgstr ""

#. type: Bullet: '1. '
#: README.md
#, markdown-text
msgid "If this is the first time you've plugged in a new Butter Box it may take several minutes before you first see the WiFi network.  After this first boot, it should boot much more quickly."
msgstr ""

#. type: Bullet: '2. '
#: README.md
#, markdown-text
msgid "Unplugging the Butter Box, waiting 10 seconds, then plugging it back in will resolve most problems."
msgstr ""

#. type: Bullet: '3. '
#: README.md
#, markdown-text
msgid "If you're comfortable in linux, `ssh` into the Butter Box and check the status of the troublesome services.  `systemctl` and `journalctl` are good tools, and the major services are:"
msgstr ""

#. type: Bullet: '	* '
#: README.md
#, markdown-text
msgid "`lighttpd`: the webserver that hosts the homepage and the admin interface for RaspAP"
msgstr ""

#. type: Bullet: '	* '
#: README.md
#, markdown-text
msgid "`hostapd`: broadcasts the local access point."
msgstr ""

#. type: Bullet: '	* '
#: README.md
#, markdown-text
msgid "`ipfs`: the local IPFS node"
msgstr ""

#. type: Bullet: '	* '
#: README.md
#, markdown-text
msgid "`dendrite`: the chat server."
msgstr ""

#. type: Bullet: '4. '
#: README.md
#, markdown-text
msgid "You can always re-image your Butter Box using the instructions above.  This will erase any data (e.g. chat, IPFS uploads) and user accounts already on the Butter Box, so only do this as a last resort or if none of your users need data from the past.  If you're unable to do this, reach out to the team who may be able to send you a postcard containing a new MicroSD card."
msgstr ""

#. type: Title ##
#: README.md
#, markdown-text, no-wrap
msgid "Onboard IPFS"
msgstr ""

#. type: Plain text
#: README.md
#, markdown-text
msgid "Note: This feature is turned off by default."
msgstr ""

#. type: Plain text
#: README.md
#, markdown-text
msgid "Butter Box comes with a built-in [IPFS](https://ipfs.tech/) node and a web UI for uploading files to IPFS.  When a user uploads a file, the the local node will pin the file and return the hash of the newly uploaded file to the user."
msgstr ""

#. type: Title ###
#: README.md
#, markdown-text, no-wrap
msgid "Downloading and viewing uploaded files"
msgstr ""

#. type: Plain text
#: README.md
#, markdown-text
msgid "You can use the IPFS CLI to list the files pinned by the Butter Box.  SSH into the box and use `ipfs pin ls --type=recursive` to see the list of CIDs.  Alternatively, you can use `$ curl -X POST \"http://butterbox.lan:5001/api/v0/pin/ls?type=recursive\"` from your local machine connected to a butter box to get a JSON formatted list of pins."
msgstr ""

#. type: Plain text
#: README.md
#, markdown-text, no-wrap
msgid "To download files, you can use your local IPFS client.  Running `ipfs get <CID>` will download the file to your computer, albeit without an extension.  If you know the filename and extension that should be applied to that CID, great!  If you don't, your browser will often recognize the filetype.  Drag the file into your browser to preview and/or save the file with an appropriate extension.  Alternatively, you can use `➜ curl -X POST \"http://butterbox.lan:5001/api/v0/get?arg=<CID>\" --output <filename.zip>` to get a ZIP containing the CID's file.\n"
msgstr ""

#. type: Title ###
#: README.md
#, markdown-text, no-wrap
msgid "Making pinned files available on the IPFS mainnet"
msgstr ""

#. type: Plain text
#: README.md
#, markdown-text
msgid "If (or when) the Butter Box is connected to the Internet, it will make the file available on the main IPFS network.  If you want these CIDs to remain accessible after the Butter Box is disconnected and returned to its offline home, you'll need to ask a pinning service to pin those CIDs."
msgstr ""

#. type: Title #
#: USERGUIDE.md
#, markdown-text, no-wrap
msgid "Butter Box User Guide"
msgstr ""

#. type: Title ##
#: USERGUIDE.md
#, markdown-text, no-wrap
msgid "What is a Butter Box?"
msgstr ""

#. type: Plain text
#: USERGUIDE.md
#, markdown-text, no-wrap
msgid "When you can't access the Internet, a Butter Box makes life offline a bit smoother.  \n"
msgstr ""

#. type: Plain text
#: USERGUIDE.md
#, markdown-text
msgid "A Butter Box is a device you can access using the wifi connection on your smartphone or computer.  When you're connected, you can install Android apps like Open Street Maps, a Survival Manual, or even games to occupy your children (or yourself!).  You can also use the Butter Box to chat publically or privately."
msgstr ""

#. type: Title ##
#: USERGUIDE.md
#, markdown-text, no-wrap
msgid "Setting up a Butter Box"
msgstr ""

#. type: Plain text
#: USERGUIDE.md
#, markdown-text
msgid "Simply plug it in using the included power adapter or a suitable micro USB power source.  In a few seconds, you'll see it's broadcasting the wifi network \"butterbox\"."
msgstr ""

#. type: Plain text
#: USERGUIDE.md
#, markdown-text
msgid "![Plug the butter box into an outlet](images/plugged-in-to-outlet.jpg){width=40%}"
msgstr ""

#. type: Plain text
#: USERGUIDE.md
#, markdown-text
msgid "![Plug the butter box into a solar power supply](images/plugged-in-to-solar.jpg){width=40%}"
msgstr ""

#. type: Plain text
#: USERGUIDE.md
#, markdown-text
msgid "The Butter Box works without an Internet connection and comes pre-loaded with everything it needs.  The first time you plug it in, it may take several minutes to start up.  After that, it will start faster."
msgstr ""

#. type: Title ##
#: USERGUIDE.md
#, markdown-text, no-wrap
msgid "Connecting to the Butter Box"
msgstr ""

#. type: Plain text
#: USERGUIDE.md
#, markdown-text
msgid "Using your phone or computer, connect to the \"butterbox\" wifi network.  To make sure your device seeks information from the Butter Box instead of the broader internet, turn off your phone's cellular connection.  You can also join the network using this QR Code:"
msgstr ""

#. type: Plain text
#: USERGUIDE.md
#, markdown-text
msgid "![Connect to the butterbox wifi network](images/en/qr/ssid-butterbox.png){width=40%}"
msgstr ""

#. type: Plain text
#: USERGUIDE.md
#, markdown-text
msgid "Next, visit `http://butterbox.lan`.  You can visit that address by scanning this QR code:"
msgstr ""

#. type: Plain text
#: USERGUIDE.md
#, markdown-text
msgid "![Visit the Butter Box's homepage](images/en/qr/butterbox.lan.png){width=40%}"
msgstr ""

#. type: Title ###
#: USERGUIDE.md
#, markdown-text, no-wrap
msgid "Installing Android Apps"
msgstr ""

#. type: Plain text
#: USERGUIDE.md
#, markdown-text
msgid "Once you've connected and loaded the Butter Box homepage, click \"Download Android App\" then click \"Download Android App\" again from the modal.  This will download a file called \"butter.apk\" to your device."
msgstr ""

#. type: Plain text
#: USERGUIDE.md
#, markdown-text
msgid "![Download butter.](images/en/download.jpg){width=40%}"
msgstr ""

#. type: Plain text
#: USERGUIDE.md
#, markdown-text
msgid "Open \"butter.apk\".  You may need to allow installation from unknown sources.  This will install the Butter App, which you can now open."
msgstr ""

#. type: Plain text
#: USERGUIDE.md
#, markdown-text
msgid "![The butter app on the homepage](images/en/app.jpg){width=40%}"
msgstr ""

#. type: Plain text
#: USERGUIDE.md
#, markdown-text
msgid "After the Butter App loads the repository, you'll see a list of the available apps.  Click any app to read more and then \"Install\" to install the app to your device."
msgstr ""

#. type: Plain text
#: USERGUIDE.md
#, markdown-text
msgid "![The butter app store listing apps](images/en/butter-app-store.jpg){width=40%}"
msgstr ""

#. type: Title ###
#: USERGUIDE.md
#, markdown-text, no-wrap
msgid "Chatting with other Butter Box users"
msgstr ""

#. type: Plain text
#: USERGUIDE.md
#, markdown-text
msgid "Once you've connected and loaded the Butter Box homepage, you can choose to join a public chat room or start a private chat using the buttons on the http://butterbox.lan homepage."
msgstr ""

#. type: Plain text
#: USERGUIDE.md
#, markdown-text
msgid "Like a bulletin board, you can see all the messages others have left on the public chat and any messages you leave on the public chat will be available to others who connect later."
msgstr ""

#. type: Plain text
#: USERGUIDE.md
#, markdown-text, no-wrap
msgid "If you want to have a private conversation, you can create your own chatroom.  When you create a private room, only those you invite can enter and all the messages you send will be encrypted so only you and your invitees can see them.  Not even the Butter Box's creators or administrators can see your private messages.  \n"
msgstr ""

#. type: Plain text
#: USERGUIDE.md
#, markdown-text
msgid "To invite others to the chatroom you created, click the icon at the top left, then click the small QR code to enlarge the QR code so your friends can scan it.  Your friends join the room by connecting to the Butter Box wifi then scanning the QR code."
msgstr ""

#. type: Plain text
#: USERGUIDE.md
#, markdown-text
msgid "If you've connected to the wifi, but can't get the homepage to load, make sure your cellular data is off."
msgstr ""

#. type: Plain text
#: USERGUIDE.md
#, markdown-text
msgid "If the Butter Box isn't working right, the easiest step is to restart it by unplugging it, waiting 10 seconds, and plugging it back in."
msgstr ""

#. type: Plain text
#: USERGUIDE.md
#, markdown-text
msgid "If restarting the Butter Box doesn't fix it, ask your administrator for help."
msgstr ""
